// composables/useModal.ts
import { ref, Ref } from "@nuxtjs/composition-api";

interface ModalComposables {
  isVisible: Ref<boolean>;
  openModal: () => void;
  closeModal: () => void;
}

export default function useModal(): ModalComposables {
  const isVisible = ref(false);

  function openModal(): void {
    isVisible.value = true;
  }

  function closeModal(): void {
    isVisible.value = false;
  }

  return {
    isVisible,
    openModal,
    closeModal,
  };
}
